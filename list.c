  1 #include<stdio.h>
  2 #include<dirent.h>
  3 #include<stdlib.h>
  4 
  5 void list(char dir[])
  6 {
            int c=0;
  7         DIR *p;
  8         struct dirent *pdir;
  9         if((p=opendir(dir))==NULL){
 10                 perror("read dir");
 11                 exit(-1);
 12         }
 13         else{
 14                 while((pdir=readdir(p))!=NULL){
 15                         printf("%s\n",pdir->d_name);
                            c++;
 16                 }
 17
 18         }
            printf("No of files in a Directory = %d\n",c);
            closedir(p);
 19 }
 20 void main(int argc,char *argv[])
 21 {
 22         if(argc==1)
 23         {
 24                 list(".");
 25         }
 26         else{
 27                 while(argc--)
 28                 {
 29                         printf("arg=%s\n",*++argv);
 30                         list(*argv);
 31                 }
 32         }
 33 }
